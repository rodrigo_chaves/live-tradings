$(document).ready(function(){
  var api_url = '//academy.jockeybit.com/api';
  var isLoadingHistory = false;

  var populateCurrentTrading = function(video){
    $('#currentVideo').attr('src',video.url);
  };

  var blockVideo = function(video){
    return '<a href="'+video.url+'" class="popup-youtube single_item link development col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">'+
              '<i class="fa fa-play" aria-hidden="true"></i>'+
              '<img src="' + ((video.picture==null)? '/img/capa.png' : video.picture )+ '" alt="">'+
            '</a>';
  };

  var populateMonthHistory = function(videos){
   
    for(var i = 0; i<videos.length; i++){
      video = blockVideo(videos[i]);

      $('#videos-container').append(video);

    }

  };

  var loadCurrentTrading = function(){
    $.get(api_url + '/livetrading',function(data){
      if(data.data!=null){
        populateCurrentTrading(data.data);
      }
    });
  };

  var loadMonthHistory = function(month_year){
    
    $('#videos-container').children().remove();

    if(!isLoadingHistory){
      isLoadingHistory = true;
      

      $.get(api_url + '/livetradingsmonth/' + month_year,function(data){
        //console.log(data);
        if(data.data!=null){
          populateMonthHistory(data.data);
        }

        isLoadingHistory = false;
      });
    }
  
  };

  loadCurrentTrading();

  //var date = new Date();
  //var month = date.getMonth()+1;
  loadMonthHistory('');

  $('.filter-videos').click(function(evt){
    evt.preventDefault();

    var filter = $(this).data('month');
    if(filter!='*'){
      loadMonthHistory($(this).data('month')  + '/' + $(this).data('year') );
    }
    else{
      loadMonthHistory('');
    }

    $('.select-cat').removeClass('select-cat');
    $(this).addClass('select-cat');
  });
});
